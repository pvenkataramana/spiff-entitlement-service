package com.precorconnect.spiffentitlementservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserIdImpl;
import com.precorconnect.spiffentitlementservice.api.SpiffEntitlementService;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.spiffentitlementservice.webapiobjectmodel.SpiffEntitlementWebDto;
import com.precorconnect.spiffentitlementservice.webapiobjectmodel.SpiffEntitlementWebView;


@RestController
@RequestMapping("/spiff-entitlements")
@Api(value = "/spiff-entitlements", description = "Operations on spiff entitlements")
public class SpiffEntitlementResource {

    /*
    fields
     */
    private final SpiffEntitlementService spiffEntitlementService;

    private final SpiffEntitlementsWebResponseFactory spiffEntitlementsWebResponseFactory;

    private final SpiffEntitlementsWebRequestFactory spiffEntitlementsWebRequestFactory;

    private final OAuth2AccessTokenFactory oAuth2AccessTokenFactory;

    @Inject
    public SpiffEntitlementResource(
            @NonNull final SpiffEntitlementService spiffEntitlementService,
            @NonNull final SpiffEntitlementsWebResponseFactory spiffEntitlementsWebResponseFactory,
            @NonNull final SpiffEntitlementsWebRequestFactory spiffEntitlementsWebRequestFactory,
            @NonNull final OAuth2AccessTokenFactory oAuth2AccessTokenFactory
    ) {

    	this.spiffEntitlementService =
                guardThat(
                        "spiffEntitlementService",
                        spiffEntitlementService
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffEntitlementsWebResponseFactory =
                guardThat(
                        "spiffEntitlementsWebResponseFactory",
                        spiffEntitlementsWebResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffEntitlementsWebRequestFactory =
                guardThat(
                        "spiffEntitlementsWebRequestFactory",
                        spiffEntitlementsWebRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.oAuth2AccessTokenFactory =
                guardThat(
                        "oAuth2AccessTokenFactory",
                        oAuth2AccessTokenFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "create/add list spiff entitlements")
    public List<Long> createSpiffEntitlements(
    		@RequestBody List<SpiffEntitlementWebDto> webSpiffentitlementDtos,
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException, AuthorizationException {

    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

    	List<SpiffEntitlementDto> coreSpiffentitlementDtos = new ArrayList<SpiffEntitlementDto>();

    	for(SpiffEntitlementWebDto viewObject: webSpiffentitlementDtos){

    		coreSpiffentitlementDtos
    				.add(spiffEntitlementsWebRequestFactory
    						.construct(
    								viewObject
    								)
    					);
    	}



        List<Long> entitlementIds = spiffEntitlementService
        						.createSpiffEntitlements(
        								coreSpiffentitlementDtos,
										accessToken
        								).stream()
										 .map(id -> id.getValue())
										 .collect(Collectors.toList());


        return
        		entitlementIds;

    }

    @RequestMapping(value="/{accountId}", method = RequestMethod.GET)
    @ApiOperation(value = "Lists all spiff entitlements with specified partner Id")
    public List<SpiffEntitlementWebView> listEntitlementsWithPartnerId(
    		@PathVariable("accountId") String accountId,
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException, AuthorizationException {

    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);


    	return
    		spiffEntitlementService
    			 	.listEntitlementsWithPartnerId(
    			 						new AccountIdImpl(accountId),
    			 						accessToken
    			 						)
    			 						.stream()
    			 						.map(spiffEntitlementsWebResponseFactory::construct)
    			 						.collect(Collectors.toList()
    			 					);

    }

    @RequestMapping(value="/{partnerSaleRegistrationId}/invoiceurl", method = RequestMethod.POST)
    @ApiOperation(value = "update the invoice url when ever there is new invoice uploaded")
    public void updateInvoiceUrl(
    		@PathVariable("partnerSaleRegistrationId") Long partnerSaleRegistrationId,
    		@RequestBody String invoiceUrl,
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException, AuthorizationException {

    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

        		spiffEntitlementService
                        .updateInvoiceUrl(
                        		new PartnerSaleRegistrationIdImpl(partnerSaleRegistrationId),
                        		new InvoiceUrlImpl(invoiceUrl),
                        		accessToken
                        		);

    }

    @RequestMapping(value="/{partnerSaleRegistrationId}/partnerrepuserid/{partnerRepUserId}", method = RequestMethod.POST)
    @ApiOperation(value = "update the partner rep when ever there is change in the partner rep")
    public void updatePartnerRep(
    		@PathVariable("partnerSaleRegistrationId") Long partnerSaleRegistrationId,
    		@PathVariable("partnerRepUserId") String partnerRepUserId,
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException, AuthorizationException {

    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

        		spiffEntitlementService
                        .updatePartnerRep(
                        		new PartnerSaleRegistrationIdImpl(partnerSaleRegistrationId),
                        		new UserIdImpl(partnerRepUserId),
                        		accessToken
                        		);

    }

    @RequestMapping(value="/spiffentitlementids", method = RequestMethod.POST)
    @ApiOperation(value = "delete the spiffentitlement by identifying with spiffentitlementid")
    public void deleteSpiffEntitlements(
    		@RequestBody List<Long> spiffEntitlementIds,
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException, AuthorizationException {

    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

    	List<SpiffEntitlementId> spiffEntitlementIdList = spiffEntitlementIds
    															.stream()
    															.map(id-> new SpiffEntitlementIdImpl(id))
    															.collect(Collectors.toList());

        		spiffEntitlementService
                        .deleteSpiffEntitlements(
                        		spiffEntitlementIdList,
                        		accessToken
                        		);

    }

}
