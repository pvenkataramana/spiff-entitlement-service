package com.precorconnect.spiffentitlementservice.webapi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.FacilityName;
import com.precorconnect.spiffentitlementservice.objectmodel.FacilityNameImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InstallDate;
import com.precorconnect.spiffentitlementservice.objectmodel.InstallDateImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceNumber;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffAmount;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDtoImpl;

@Component
public class SpiffEntitlementsWebRequestFactoryImpl implements
		SpiffEntitlementsWebRequestFactory {

	@Override
	public SpiffEntitlementDto construct(
			com.precorconnect.spiffentitlementservice.webapiobjectmodel.
				@NonNull SpiffEntitlementWebDto spiffEntitlementWebDto) {

		AccountId accountId = new
												AccountIdImpl(
														spiffEntitlementWebDto
															.getAccountId()
													);


		PartnerSaleRegistrationId partnerSaleRegistrationId = new
																PartnerSaleRegistrationIdImpl(
																		spiffEntitlementWebDto
																			.getPartnerSaleRegistrationId()
																	);

		FacilityName facilityName = new
										FacilityNameImpl(
												spiffEntitlementWebDto
													.getFacilityName()
												);

		InvoiceNumber invoiceNumber = new
										InvoiceNumberImpl(
        										spiffEntitlementWebDto
        											.getInvoiceNumber()
        											);


		InvoiceUrl invoiceUrl = new
									InvoiceUrlImpl(
											spiffEntitlementWebDto
        										.getInvoiceUrl()
                        						);


		UserId partnerRepUserId = new
											UserIdImpl(
												spiffEntitlementWebDto
        											.getPartnerRepUserId()
                        							);

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date date;
		try {
			date = formatter.parse(spiffEntitlementWebDto
					.getInstallDate());
		} catch (ParseException e) {
			throw new RuntimeException("Date field parse exception: ",e);
		}
		InstallDate installDate = new
									InstallDateImpl(
											date
												);

		SpiffAmount spiffAmount = new
									SpiffAmountImpl(
											spiffEntitlementWebDto
												.getSpiffAmount()
											);


        return
                new SpiffEntitlementDtoImpl(
                		accountId,
                		partnerSaleRegistrationId,
                		facilityName,
                		invoiceNumber,
                		invoiceUrl,
                		partnerRepUserId,
                		installDate,
                		spiffAmount
                );
	}

}
