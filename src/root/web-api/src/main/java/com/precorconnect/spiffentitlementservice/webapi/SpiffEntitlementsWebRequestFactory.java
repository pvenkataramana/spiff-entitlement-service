package com.precorconnect.spiffentitlementservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface SpiffEntitlementsWebRequestFactory {

	com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto construct(
    		com.precorconnect.spiffentitlementservice.webapiobjectmodel.
    		  @NonNull SpiffEntitlementWebDto spiffEntitlementWebDto
    );
	
}
