package com.precorconnect.spiffentitlementservice.webapi;

import javax.inject.Singleton;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.precorconnect.spiffentitlementservice.api.SpiffEntitlementService;
import com.precorconnect.spiffentitlementservice.api.SpiffEntitlementServiceConfigFactoryImpl;
import com.precorconnect.spiffentitlementservice.api.SpiffEntitlementServiceImpl;


@Configuration
public class SpiffEntitlementServiceBean {
	
    @Bean
    @Singleton
    public SpiffEntitlementService spiffEntitlementService() {

        return
                new SpiffEntitlementServiceImpl(
                		new SpiffEntitlementServiceConfigFactoryImpl()
                			                                   .construct()
                );

    }
}
