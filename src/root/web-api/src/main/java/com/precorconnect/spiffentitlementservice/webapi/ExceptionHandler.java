package com.precorconnect.spiffentitlementservice.webapi;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class ExceptionHandler {

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @org.springframework.web.bind.annotation.
            ExceptionHandler(value = AuthenticationException.class)
    public void authenticationException() {
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @org.springframework.web.bind.annotation.
            ExceptionHandler(value = AuthorizationException.class)
    public void authorizationException() {
    }

}
