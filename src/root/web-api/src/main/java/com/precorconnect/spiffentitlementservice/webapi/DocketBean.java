package com.precorconnect.spiffentitlementservice.webapi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import javax.inject.Singleton;

@Configuration
class DocketBean {

    @Bean
    @Singleton
    public Docket docket() {
        return new Docket(
                DocumentationType.SWAGGER_2
        )
                .select()
                .apis(RequestHandlerSelectors.basePackage(getClass().getPackage().getName()))
                .build()
                .apiInfo(
                        new ApiInfo(
                                "Precorconnect Spiff entitlement",
                                "",
                                "1",
                                "",
                                "",
                                "",
                                ""
                        )
                )
                .enableUrlTemplating(true);
    }

}
