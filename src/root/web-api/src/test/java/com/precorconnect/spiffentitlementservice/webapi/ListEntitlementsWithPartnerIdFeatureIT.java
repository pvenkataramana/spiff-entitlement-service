package com.precorconnect.spiffentitlementservice.webapi;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"features/ListEntitlementsWithPartnerId.feature"},
        glue = {"com.precorconnect.spiffentitlementservice.webapi.listentitlementswithpartneridfeature"}
        )
public class ListEntitlementsWithPartnerIdFeatureIT {

}
