package com.precorconnect.spiffentitlementservice.webapi.updatepartnerrepfeature;

import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.spiffentitlementservice.webapi.AbstractSpringIntegrationTest;
import com.precorconnect.spiffentitlementservice.webapi.Config;
import com.precorconnect.spiffentitlementservice.webapi.ConfigFactory;
import com.precorconnect.spiffentitlementservice.webapi.Dummy;
import com.precorconnect.spiffentitlementservice.webapi.Factory;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs
		extends AbstractSpringIntegrationTest{

    /*
    fields
     */
    private final Config config =
            new ConfigFactory().construct();

    private final Dummy dummy = new Dummy();

    private final Factory factory =
            new Factory(
                    dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                            config.getIdentityServiceJwtSigningKey()
                    )
            );

    private Long partnerSaleRegistrationId;

    private String userId;

    private OAuth2AccessToken oAuth2AccessToken;

    private Response responseOfPostToSpiffEntitlements;

    @Before
    public void beforeAll() {

        RestAssured.port = getPort();

    }

    /*
    steps
     */
    @Given("^a partnerSaleRegistrationId and userId consists of:$")
    public void anaccountIdconsistsof(
            DataTable attributes
    ) throws Throwable {

        // no op , input prepared in dummy

    }

    @Given("^I provide an accessToken identifying me as a partner rep$")
    public void provideanaccessTokenidentifyingmeasapartnerrep(
    ) throws Throwable {

        oAuth2AccessToken =
               factory.constructValidAppOAuth2AccessToken();

    }

   @And("^provide a valid partnerSaleRegistrationId and userid$")
   public void provideavalidpartnerSaleRegistrationIdanduserid(
		   )throws Throwable{

	   partnerSaleRegistrationId =
			   dummy
			   .getSpiffEntitlementWebDto()
			   .getPartnerSaleRegistrationId();

	   userId =
			   dummy
			   .getSpiffEntitlementWebDto()
			   .getPartnerRepUserId();
   }

   @When("^I post a request to updatePartnerRep$")
   public void IpostarequesttoupdatePartnerRep(
		   )throws Throwable{

	   responseOfPostToSpiffEntitlements =
   			given()
               .contentType(ContentType.JSON)
               .header(
                       "Authorization",
                       String.format(
                               "Bearer %s",
                               oAuth2AccessToken
                                       .getValue()
                       )
               )
               .post("/spiff-entitlements/"
            		   +partnerSaleRegistrationId
            		   +"/partnerrepuserid/"
            		   +userId
            		   );
   }

   @Then("^the userId of the matched partnerSaleRegistrationId are modified$")
   public void theuserIdofthematchedpartnerSaleRegistrationIdaremodified(
		   )throws Throwable{

	   responseOfPostToSpiffEntitlements
							.then()
							.assertThat()
							.statusCode(200);
   }

}
