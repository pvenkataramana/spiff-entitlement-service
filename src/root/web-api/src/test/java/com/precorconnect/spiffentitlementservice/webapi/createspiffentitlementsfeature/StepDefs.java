package com.precorconnect.spiffentitlementservice.webapi.createspiffentitlementsfeature;

import static com.jayway.restassured.RestAssured.given;

import java.util.List;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.spiffentitlementservice.webapi.AbstractSpringIntegrationTest;
import com.precorconnect.spiffentitlementservice.webapi.Config;
import com.precorconnect.spiffentitlementservice.webapi.ConfigFactory;
import com.precorconnect.spiffentitlementservice.webapi.Dummy;
import com.precorconnect.spiffentitlementservice.webapi.Factory;
import com.precorconnect.spiffentitlementservice.webapiobjectmodel.SpiffEntitlementWebDto;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class StepDefs
		extends AbstractSpringIntegrationTest {

    /*
    fields
     */
    private final Config config =
            new ConfigFactory().construct();

    private final Dummy dummy = new Dummy();

    private final Factory factory =
            new Factory(
                    dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                            config.getIdentityServiceJwtSigningKey()
                    )
            );

    private OAuth2AccessToken oAuth2AccessToken;

    private List<SpiffEntitlementWebDto> spiffEntitlementWebDtos;

    private Response responseOfPostToSpiffEntitlements;


    @Before
    public void beforeAll() {

        RestAssured.port = getPort();

    }

    /*
    steps
     */
    @Given("^a spiffEntitlementDtoList consists of:$")
    public void aspiffEntitlementDtoListconsistsof(
            DataTable attributes
    ) throws Throwable {

        // no op , input prepared in dummy

    }

    @Given("^I provide an accessToken identifying me as a partner rep$")
    public void provideanaccessTokenidentifyingmeasapartnerrep(
    ) throws Throwable {

        oAuth2AccessToken =
                factory.constructValidAppOAuth2AccessToken();

    }

    @And("^provide a valid spiffEntitlementWebDtoList$")
    public void provideavalidspiffEntitlementWebDtoList(
    ) throws Throwable {

    	spiffEntitlementWebDtos =
                dummy.getSpiffEntitlementWebDtos();

    }

    @When("^I post to /spiff-entitlements$")
    public void posttospiffentitlements(
    		) throws Throwable {
    	responseOfPostToSpiffEntitlements =
    			given()
                .contentType(ContentType.JSON)
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                oAuth2AccessToken
                                        .getValue()
                        )
                )
                .body(spiffEntitlementWebDtos)
                .post("/spiff-entitlements");
    }

    @Then("^spiffEntitlementWebDtoList is added to the spiffentitlement database with attributes:$")
    public void spiffEntitlementWebDtoListisaddedtothespiffentitlementdatabasewithattributes(
    		DataTable arg
    		){

    	responseOfPostToSpiffEntitlements
    			.then()
    			.assertThat()
    			.statusCode(200);

    }

}
