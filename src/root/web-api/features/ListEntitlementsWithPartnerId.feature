Feature: List Entitlements With Partner Id
  List Entitlements With Partner Id

  Background:
    Given an accountId consists of:
      | attribute     				| validation | type   |
      | accountId     				| required   | string |
      
  Scenario: Success
    Given I provide an accessToken identifying me as a partner rep
    And provide a valid accountId
    When I make a get request to /spiff-entitlements
    Then the spiffentitlements with the matched accountid are returned
