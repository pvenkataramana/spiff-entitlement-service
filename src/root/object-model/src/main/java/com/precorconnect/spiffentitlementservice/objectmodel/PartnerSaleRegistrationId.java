package com.precorconnect.spiffentitlementservice.objectmodel;

public interface PartnerSaleRegistrationId {

	public Long getValue();

}
