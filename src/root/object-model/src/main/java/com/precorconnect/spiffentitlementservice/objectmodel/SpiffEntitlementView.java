package com.precorconnect.spiffentitlementservice.objectmodel;

import com.precorconnect.AccountId;
import com.precorconnect.UserId;

public interface SpiffEntitlementView {

	SpiffEntitlementId getSpiffEntitlementId();

    AccountId getAccountId();

    PartnerSaleRegistrationId getPartnerSaleRegistrationId();

    FacilityName getFacilityName();

    InvoiceNumber getInvoiceNumber();

    InvoiceUrl getInvoiceUrl();

    UserId getPartnerRepUserId();

    InstallDate getInstallDate();

    SpiffAmount getSpiffAmount();

}
