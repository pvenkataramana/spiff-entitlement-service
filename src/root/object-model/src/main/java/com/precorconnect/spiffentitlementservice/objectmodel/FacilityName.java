package com.precorconnect.spiffentitlementservice.objectmodel;

public interface FacilityName {

	public String getValue();

}
