package com.precorconnect.spiffentitlementservice.objectmodel;

public interface InvoiceNumber {

	public String getValue();

}
