package com.precorconnect.spiffentitlementservice.database;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.Password;
import com.precorconnect.PasswordImpl;
import com.precorconnect.UserIdImpl;
import com.precorconnect.Username;
import com.precorconnect.UsernameImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.FacilityNameImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InstallDateImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDtoImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementViewImpl;

public class Dummy {

    /*
    fields
     */
    private final URI uri;

    private final Username username =
            new UsernameImpl("username");

    private final Password password =
            new PasswordImpl("password");

    private String accountId = "109837372893488727";

    private Long partnerSaleRegistrationId = 12L;

    private String facilityName = "Ram";

    private String invoiceNumber = "1234";

    private String invoiceUrl = "www.testinvoice.com";

    private String partnerRepUserId = "23";

    private String installDate = "11/21/1999";

    private Double spiffAmount = 100.00;

    private Long spiffEntitlementId = 1L;

    private SpiffEntitlementDto spiffEntitlementDto;

    private List<SpiffEntitlementDto> spiffEntitlementDtosList = new ArrayList<SpiffEntitlementDto>();

    private Date date;

    private SpiffEntitlement spiffEntitlement;

    private SpiffEntitlementView spiffEntitlementView;

    private String newInvoiceUrl = "http://www.yahoo.com/test";

    private String newUserId = "222";

	/*
    constructors
     */
    public Dummy() {
        try {

            uri = new URI("http://dev.precorconnect.com");

            try {
				date = new SimpleDateFormat("MM/dd/yyyy").parse(installDate);
			} catch (ParseException e) {
				throw new RuntimeException("date parsing exception: ", e);
			}


            spiffEntitlementDto = new SpiffEntitlementDtoImpl(
            										new AccountIdImpl(accountId),
            										new PartnerSaleRegistrationIdImpl(partnerSaleRegistrationId),
            										new FacilityNameImpl(facilityName),
            										new InvoiceNumberImpl(invoiceNumber),
            										new InvoiceUrlImpl(invoiceUrl),
            										new UserIdImpl(partnerRepUserId),
            										new InstallDateImpl(date),
            										new SpiffAmountImpl(spiffAmount)
            									  );

            spiffEntitlementDtosList.add(spiffEntitlementDto);

            spiffEntitlement = new SpiffEntitlement();
            spiffEntitlement.setSpiffEntitlementId(spiffEntitlementId);
            spiffEntitlement.setAccountId(accountId);
            spiffEntitlement.setPartnerSalRegistrationId(partnerSaleRegistrationId);
            spiffEntitlement.setFacilityName(facilityName);
            spiffEntitlement.setInvoiceNumber(invoiceNumber);
            spiffEntitlement.setInvoiceUrl(invoiceUrl);
            spiffEntitlement.setPartnerRepId(partnerRepUserId);
            spiffEntitlement.setInstallDate(date);
            spiffEntitlement.setSpiffAmount(spiffAmount);

            spiffEntitlementView = new SpiffEntitlementViewImpl(
            		new SpiffEntitlementIdImpl(spiffEntitlementId),
            		new AccountIdImpl(accountId),
					new PartnerSaleRegistrationIdImpl(partnerSaleRegistrationId),
					new FacilityNameImpl(facilityName),
					new InvoiceNumberImpl(invoiceNumber),
					new InvoiceUrlImpl(invoiceUrl),
					new UserIdImpl(partnerRepUserId),
					new InstallDateImpl(date),
					new SpiffAmountImpl(spiffAmount)
            		);
        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }
    }

    /*
    getter methods
     */
    public URI getUri() {
        return uri;
    }

    public Username getUsername() {
        return username;
    }

    public Password getPassword() {
        return password;
    }

	public SpiffEntitlementDto getSpiffEntitlementDto() {
		return spiffEntitlementDto;
	}

	public List<SpiffEntitlementDto> getSpiffEntitlementDtosList() {
		return spiffEntitlementDtosList;
	}

	public Date getDate() {
		return date;
	}

    public String getAccountId() {
		return accountId;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public String getInvoiceUrl() {
		return invoiceUrl;
	}

	public String getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public String getInstallDate() {
		return installDate;
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}

	public SpiffEntitlement getSpiffEntitlement() {
		return spiffEntitlement;
	}

	public SpiffEntitlementView getSpiffEntitlementView() {
		return spiffEntitlementView;
	}

	public String getNewInvoiceUrl() {
		return newInvoiceUrl;
	}

	public String getNewUserId() {
		return newUserId;
	}



}
