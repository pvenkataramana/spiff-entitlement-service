package com.precorconnect.spiffentitlementservice.database;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public class SpiffEntitlementResponseFactoryImplTest {

	/*
    fields
	*/
    private  final Dummy dummy = new Dummy();

    private final SpiffEntitlementsResponseFactoryImpl spiffEntitlementsResponseFactoryImpl =
    														new SpiffEntitlementsResponseFactoryImpl();

    /*
    test methods
	*/
    @Test(expected = IllegalArgumentException.class)
    public void testConstruct_whenSpiffEntitlementNull_shouldThrowIllegalArgumentException(
    		)throws Exception{

    	spiffEntitlementsResponseFactoryImpl
    						.construct(
    								null
    								);

    }

    @Test
    public void testConstruct_whenSpiffEntitlementDtoFromDummy_shouldReturnSpiffEntitlementEntityObj(
    		){
    	SpiffEntitlementView resultSpiffEntitlementView = spiffEntitlementsResponseFactoryImpl
																				.construct(
																						dummy.getSpiffEntitlement()
																						);

    	assertEquals(
    			dummy.getSpiffEntitlementView(),
    			resultSpiffEntitlementView
    			);

    }
}
