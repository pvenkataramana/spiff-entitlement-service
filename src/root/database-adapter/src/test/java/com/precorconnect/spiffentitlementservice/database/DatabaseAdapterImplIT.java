package com.precorconnect.spiffentitlementservice.database;

import static org.assertj.core.api.StrictAssertions.assertThat;

import java.util.Collection;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.precorconnect.UserIdImpl;
import com.precorconnect.spiffentitlementservice.core.DatabaseAdapter;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DatabaseAdapterImplIT {

	/*
    fields
	*/
	private final Dummy dummy =
   		 new Dummy();

    private final Config config =
    			new ConfigFactory()
    		                      .construct();

    /*
    test methods
	*/
    @Test
    public void testCreateSpiffEntitlements_whenEntitlementDtoList_shouldReturnsEntitlementIds(
    ) throws Exception {

        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()
                );

        Collection<SpiffEntitlementId>
                entitlementIds =
                				objectUnderTest
                				.createSpiffEntitlements(
                						dummy.getSpiffEntitlementDtosList()
                						);

        assertThat(entitlementIds.size())
                .isGreaterThan(0);
    }

    @Test
    public void testListEntitlementsWithPartnerId_whenAccountId_shouldReturnsListOfEntitlements(
    ) throws Exception {

        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()
                );

        Collection<SpiffEntitlementView>
                listEntitlements =
                			objectUnderTest
                			.listEntitlementsWithPartnerId(
                					dummy
                						.getSpiffEntitlementDto()
                						.getAccountId()
                					);

        assertThat(listEntitlements.size())
                .isGreaterThan(0);
    }

    @Test
    public void testUpdateInvoiceUrl_whenNewInvoiceUrl_shouldReturnsNothing(
    ) throws Exception {

        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()
                );

        objectUnderTest
                	.updateInvoiceUrl(
                			dummy
                				.getSpiffEntitlementDto()
                				.getPartnerSaleRegistrationId(),
                			new InvoiceUrlImpl(
                					dummy.getNewInvoiceUrl()
                					)
                			);
    }

    @Test
    public void testUpdatePartnerRep_whenPartnerRepUserId_shouldReturnsNothing(
    ) throws Exception {

        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()
                );

        objectUnderTest.updatePartnerRep(
        		dummy
        			.getSpiffEntitlementDto()
        			.getPartnerSaleRegistrationId(),
        		new UserIdImpl(
        				dummy.getNewUserId()
        				)
        		);

    }
}
