package com.precorconnect.spiffentitlementservice.database;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SpiffEntitlementsRequestFactroyImplTest {

	/*
    fields
	*/
    private  Dummy dummy = new Dummy();

    private final SpiffEntitlementsRequestFactoryImpl spiffEntitlementsRequestFactoryImpl =
    														new SpiffEntitlementsRequestFactoryImpl();

    /*
    test methods
	*/
    @Test(expected = IllegalArgumentException.class)
    public void testConstruct_whenSpiffEntitlementDtoNull_shouldThrowIllegalArgumentException(
    		)throws Exception{

    	spiffEntitlementsRequestFactoryImpl
    						.construct(
    								null
    								);

    }

    @Test
    public void testConstruct_whenSpiffEntitlementDtoFromDummy_shouldReturnSpiffEntitlementEntityObj(
    		){
    	SpiffEntitlement resultSpiffEntitlement = spiffEntitlementsRequestFactoryImpl
																				.construct(
																						dummy.getSpiffEntitlementDto()
																						);

    	assertEquals(
    			dummy.getSpiffEntitlement(),
    			resultSpiffEntitlement
    			);

    }
}
