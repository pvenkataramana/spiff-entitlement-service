package com.precorconnect.spiffentitlementservice.database;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.flywaydb.core.Flyway;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.UserId;
import com.precorconnect.spiffentitlementservice.core.DatabaseAdapter;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public class DatabaseAdapterImpl
		implements DatabaseAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public DatabaseAdapterImpl(
            @NonNull DatabaseAdapterConfig config
    ) {

        // ensure database schema up to date
        Flyway flyway = new Flyway();
        flyway.setDataSource(
                config.getUri().toString(),
                config.getUsername().getValue(),
                config.getPassword().getValue());
        flyway.migrate();


        GuiceModule guiceModule =
                new GuiceModule(
                        config
                );

        injector =
                Guice.createInjector(guiceModule);

    }

	@Override
	public Collection<SpiffEntitlementId> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlements) {

		return
                injector
                        .getInstance(CreateSpiffEntitlementFeature.class)
                        .createSpiffEntitlements(spiffEntitlements);
	}

	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId accountId) {

		return
                injector
                        .getInstance(ListSpiffEntitlementFeature.class)
                        .listEntitlementsWithPartnerId(accountId);
	}

	@Override
	public void updateInvoiceUrl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl) {

		injector
        		.getInstance(UpdateInvoiceUrlFeature.class)
        		.updateInvoiceUrl(partnerSaleRegistrationId, invoiceUrl);

	}

	@Override
	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId partnerRepUserId) {
		injector
				.getInstance(UpdatePartnerRepFeature.class)
				.updatePartnerRep(partnerSaleRegistrationId, partnerRepUserId);

	}

	@Override
	public void deleteSpiffEntitlements(
		 	@NonNull List<SpiffEntitlementId> spiffEntitlementIds
		 	){

		injector
				.getInstance(DeleteSpiffEntitlementsFeature.class)
				.deleteSpiffEntitlements(spiffEntitlementIds);
	}

}
