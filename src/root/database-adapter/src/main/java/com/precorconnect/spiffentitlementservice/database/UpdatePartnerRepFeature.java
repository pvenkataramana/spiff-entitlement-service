package com.precorconnect.spiffentitlementservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.UserId;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;

public interface UpdatePartnerRepFeature {

	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId delaerRepUserid);

}
