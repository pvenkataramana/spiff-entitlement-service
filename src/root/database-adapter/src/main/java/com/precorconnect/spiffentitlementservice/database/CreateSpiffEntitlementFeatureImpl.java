package com.precorconnect.spiffentitlementservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementIdImpl;

@Singleton
public class CreateSpiffEntitlementFeatureImpl
		implements CreateSpiffEntitlementFeature {

	 private final SessionFactory sessionFactory;
	 private final SpiffEntitlementsRequestFactory spiffEntitlementsRequestFactory;

    @Inject
    public CreateSpiffEntitlementFeatureImpl(
            @NonNull final SessionFactory sessionFactory,
            @NonNull final SpiffEntitlementsRequestFactory spiffEntitlemensRequestFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                        sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();


    	spiffEntitlementsRequestFactory =
                guardThat(
                        "spiffEntitlemensRequestFactory",
                        spiffEntitlemensRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<SpiffEntitlementId> createSpiffEntitlements(
											List<SpiffEntitlementDto> spiffEntitlements
											) {

		List<SpiffEntitlementId> entitlementIdsList = new ArrayList<SpiffEntitlementId>();;

		Session  session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        try {

            	for(SpiffEntitlementDto spiffEntitlement : spiffEntitlements){

            		Long entitlementId =  (Long) session.save(
            												spiffEntitlementsRequestFactory.
            													construct(
            															spiffEntitlement
            															)
            												);
            		entitlementIdsList.
            					add(
            						new SpiffEntitlementIdImpl(
            											entitlementId
            											)
            						);
                }

            	tx.commit();

        } catch(final Exception e) {

        	tx.rollback();
        	throw new RuntimeException("exception while committing transaction:", e);

        } finally {

        	if (session != null) {
                session.close();
            }

        }
        return entitlementIdsList;
	}

}
