package com.precorconnect.spiffentitlementservice.database;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "spiffentitlements")
public class SpiffEntitlement {

	 /*
    fields
     */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long spiffEntitlementId;

    private String accountId;

    private Long partnerSaleRegistrationId;

    private String facilityName;

    private String  invoiceNumber;

    private String invoiceUrl;

    private String partnerRepUserId;

    private Date installDate;

	private Double spiffAmount;

	/*
	 * getter & setter methods
	 */
	public Long getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

	public String getAccountId() {
		return accountId;
	}

	public Long getPartnerSalRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public String getInvoiceUrl() {
		return invoiceUrl;
	}

	public String getPartnerRepId() {
		return partnerRepUserId;
	}


	public Date getInstallDate() {
		return installDate;
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}

	public void setSpiffEntitlementId(Long spiffEntitlementId) {
		this.spiffEntitlementId = spiffEntitlementId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public void setPartnerSalRegistrationId(Long partnerSalRegistrationId) {
		partnerSaleRegistrationId = partnerSalRegistrationId;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public void setInvoiceUrl(String invoiceUrl) {
		this.invoiceUrl = invoiceUrl;
	}

	public void setPartnerRepId(String partnerRepId) {
		partnerRepUserId = partnerRepId;
	}

	public void setInstallDate(Date installDate) {
		this.installDate = installDate;
	}

	public void setSpiffAmount(Double spiffAmount) {
		this.spiffAmount = spiffAmount;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((facilityName == null) ? 0 : facilityName.hashCode());
		result = prime * result
				+ ((installDate == null) ? 0 : installDate.hashCode());
		result = prime * result
				+ ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
		result = prime * result
				+ ((invoiceUrl == null) ? 0 : invoiceUrl.hashCode());
		result = prime
				* result
				+ ((accountId == null) ? 0 : accountId.hashCode());
		result = prime
				* result
				+ ((partnerRepUserId == null) ? 0 : partnerRepUserId.hashCode());
		result = prime
				* result
				+ ((partnerSaleRegistrationId == null) ? 0
						: partnerSaleRegistrationId.hashCode());
		result = prime * result
				+ ((spiffAmount == null) ? 0 : spiffAmount.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SpiffEntitlement other = (SpiffEntitlement) obj;
		if (facilityName == null) {
			if (other.facilityName != null) {
				return false;
			}
		} else if (!facilityName.equals(other.facilityName)) {
			return false;
		}
		if (installDate == null) {
			if (other.installDate != null) {
				return false;
			}
		} else if (!installDate.equals(other.installDate)) {
			return false;
		}
		if (invoiceNumber == null) {
			if (other.invoiceNumber != null) {
				return false;
			}
		} else if (!invoiceNumber.equals(other.invoiceNumber)) {
			return false;
		}
		if (invoiceUrl == null) {
			if (other.invoiceUrl != null) {
				return false;
			}
		} else if (!invoiceUrl.equals(other.invoiceUrl)) {
			return false;
		}
		if (accountId == null) {
			if (other.accountId != null) {
				return false;
			}
		} else if (!accountId.equals(other.accountId)) {
			return false;
		}
		if (partnerRepUserId == null) {
			if (other.partnerRepUserId != null) {
				return false;
			}
		} else if (!partnerRepUserId.equals(other.partnerRepUserId)) {
			return false;
		}
		if (partnerSaleRegistrationId == null) {
			if (other.partnerSaleRegistrationId != null) {
				return false;
			}
		} else if (!partnerSaleRegistrationId
				.equals(other.partnerSaleRegistrationId)) {
			return false;
		}
		if (spiffAmount == null) {
			if (other.spiffAmount != null) {
				return false;
			}
		} else if (!spiffAmount.equals(other.spiffAmount)) {
			return false;
		}
		return true;
	}



}
