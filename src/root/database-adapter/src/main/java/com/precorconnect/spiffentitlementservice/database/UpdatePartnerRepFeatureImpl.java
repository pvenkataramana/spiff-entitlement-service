package com.precorconnect.spiffentitlementservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.precorconnect.UserId;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;

@Singleton
public class UpdatePartnerRepFeatureImpl implements UpdatePartnerRepFeature {

    private final SessionFactory sessionFactory;

    @Inject
    public UpdatePartnerRepFeatureImpl(
            @NonNull final SessionFactory sessionFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                        sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId partnerRepUserid
			) {

		Session  session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        try {
            Query query= session
                    		.createQuery("update SpiffEntitlement set partnerRepUserid= :partnerRepUserid"
                    						+" where partnerSaleRegistrationId= :partnerSaleRegistrationId"
                    			);
            query.setParameter("partnerRepUserid", partnerRepUserid.getValue());
            query.setParameter("partnerSaleRegistrationId", partnerSaleRegistrationId.getValue());
            query.executeUpdate();

            tx.commit();

        } catch(final Exception e) {

        	tx.rollback();
        	throw new RuntimeException("exception while committing transaction:", e);

        } finally {

            if (session != null) {
                session.close();
            }

        }

	}

}
