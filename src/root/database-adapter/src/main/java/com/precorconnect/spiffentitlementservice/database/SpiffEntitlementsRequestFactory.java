package com.precorconnect.spiffentitlementservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;

public interface SpiffEntitlementsRequestFactory {

	SpiffEntitlement construct(
			@NonNull SpiffEntitlementDto spiffEntitlement
			);
}
