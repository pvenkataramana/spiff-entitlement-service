package com.precorconnect.spiffentitlementservice.api;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"features/UpdateInvoiceUrl.feature"},
        glue = {"com.precorconnect.spiffentitlementservice.api.updateinvoiceurlfeature"}
        )
public class UpdateInvoiceUrlFeatureIT {

}
