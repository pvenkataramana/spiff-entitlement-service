package com.precorconnect.spiffentitlementservice.api;

import com.precorconnect.spiffentitlementservice.database.DatabaseAdapterConfig;
import com.precorconnect.spiffentitlementservice.identity.IdentityServiceAdapterConfig;


public interface SpiffEntitlementServiceConfig {

    DatabaseAdapterConfig getDatabaseAdapterConfig();
    
    IdentityServiceAdapterConfig getIdentityServiceAdapterConfig();
    
}
