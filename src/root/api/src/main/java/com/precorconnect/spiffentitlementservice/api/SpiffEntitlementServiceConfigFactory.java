package com.precorconnect.spiffentitlementservice.api;

public interface SpiffEntitlementServiceConfigFactory {

	SpiffEntitlementServiceConfig construct();
	
}
