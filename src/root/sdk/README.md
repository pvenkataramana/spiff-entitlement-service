## Description
Precor Connect spiff entitlement service SDK for java.

## Features

##### Create Spiff Entitlements
* [documentation](features/CreateSpiffEntitlements.feature)

##### List Spiff Entitlements With Id
* [documentation](features/ListEntitlementsWithPartnerId.feature)

##### Update Invoice Url
* [documentation](features/UpdateInvoiceUrl.feature)

##### Update Partner Rep
* [documentation](features/UpdatePartnerRep.feature)

## Maven Installation

in pom.xml
```xml
<dependencies>
    <dependency>
        <groupId>com.precorconnect.spiffentitlementservice</groupId>
        <artifactId>sdk</artifactId>
        ...
    </dependency>
    ...
</dependencies>
<repositories>
    <repository>
        <id>maven.precorconnect.com</id>
        <name>maven.precorconnect.com</name>
        <url>https://s3.amazonaws.com/maven.precorconnect.com</url>
    </repository>
    ...
</repositories>
```
