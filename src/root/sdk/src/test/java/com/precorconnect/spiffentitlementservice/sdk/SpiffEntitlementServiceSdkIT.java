package com.precorconnect.spiffentitlementservice.sdk;

import static org.assertj.core.api.StrictAssertions.assertThat;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.precorconnect.UserIdImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffentitlementservice.webapi.Application;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest({"server.port=0"})
public class SpiffEntitlementServiceSdkIT {

    /*
    fields
     */
    @Value("${local.server.port}")
    int port;

    private final Dummy dummy =
    		 new Dummy();

    private final Config config =
            new ConfigFactory()
                    .construct();

    private final Factory factory =
            new Factory(
                    new IdentityServiceIntegrationTestSdkImpl(
                    		config.getIdentityServiceJwtSigningKey()
                    ),
                    dummy
            );

    /*
    test methods
     */
    @Test
    public void createSpiffEntitlements_ReturnsOneOrMoreIds(
    ) throws Exception {

        SpiffEntitlementServiceSdk objectUnderTest =
                new SpiffEntitlementServiceSdkImpl(
                        new SpiffEntitlementServiceSdkConfigFactoryImpl()
                        										.construct()
                );

        Collection<Long>
                actualSpiffEntitlementIds =
                objectUnderTest.createSpiffEntitlements(
                		dummy.getSpiffEntitlementDtosList(),
                        factory.constructValidAppOAuth2AccessToken()
                );

        assertThat(actualSpiffEntitlementIds.size())
                .isGreaterThan(0);

    }

    @Test
    public void listSpiffEntitlements_ReturnsOneOrMoreEntitlements(
    ) throws Exception {

        SpiffEntitlementServiceSdk objectUnderTest =
                new SpiffEntitlementServiceSdkImpl(
                        new SpiffEntitlementServiceSdkConfigFactoryImpl()
                        										.construct()
                );

        Collection<SpiffEntitlementView>
                actualSpiffEntitlements =
                objectUnderTest.listEntitlementsWithPartnerId(
                		dummy.getSpiffEntitlementDto().getAccountId(),
                        factory.constructValidAppOAuth2AccessToken()
                );

        assertThat(actualSpiffEntitlements.size())
                .isGreaterThan(0);

    }

    @Test
    public void updateInvoiceUrl_ReturnsNothing(
    ) throws Exception {

        SpiffEntitlementServiceSdk objectUnderTest =
                new SpiffEntitlementServiceSdkImpl(
                        new SpiffEntitlementServiceSdkConfigFactoryImpl()
                        										.construct()
                );

         objectUnderTest.updateInvoiceUrl(
                		dummy.getSpiffEntitlementDto().getPartnerSaleRegistrationId(),
                		new InvoiceUrlImpl(dummy.getNewInvoiceUrl()),
                        factory.constructValidAppOAuth2AccessToken()
        		 		);

    }

    @Test
    public void updatePartnerRep_ReturnsNothing(
    ) throws Exception {

        SpiffEntitlementServiceSdk objectUnderTest =
                new SpiffEntitlementServiceSdkImpl(
                        new SpiffEntitlementServiceSdkConfigFactoryImpl()
                        										.construct()
                );

         objectUnderTest.updatePartnerRep(
                		dummy.getSpiffEntitlementDto().getPartnerSaleRegistrationId(),
                		new UserIdImpl(dummy.getNewUserId()),
                        factory.constructValidAppOAuth2AccessToken()
        		 		);

    }

}
