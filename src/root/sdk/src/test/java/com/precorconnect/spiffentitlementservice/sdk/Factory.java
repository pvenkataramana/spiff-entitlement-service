package com.precorconnect.spiffentitlementservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.identityservice.AppJwt;
import com.precorconnect.identityservice.AppJwtImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdk;

public class Factory {

    /*
    fields
     */
    private final IdentityServiceIntegrationTestSdk identityServiceIntegrationTestSdk;

    private final Dummy dummy;

    /*
    constructors
     */
    public Factory(
            @NonNull IdentityServiceIntegrationTestSdk identityServiceIntegrationTestSdk,
            @NonNull Dummy dummy
    ) {

        this.identityServiceIntegrationTestSdk =
				guardThat(
						"identityServiceIntegrationTestSdk",
						identityServiceIntegrationTestSdk
				)
						.isNotNull()
						.thenGetValue();

        this.dummy =
				guardThat(
						"dummy",
						dummy
				)
						.isNotNull()
						.thenGetValue();

    }

    public OAuth2AccessToken constructValidAppOAuth2AccessToken() {

        AppJwt appJwt = new AppJwtImpl(
                Instant.now().plusSeconds(480),
                dummy.getUri(),
                dummy.getUri()
        );

        return
                identityServiceIntegrationTestSdk
                        .getAppOAuth2AccessToken(appJwt);
    }

    public SpiffEntitlementServiceSdkConfig constructSpiffEntitlementServiceSdkConfig(
            Integer port
    ) {

        URL baseUrl;

        try {

            baseUrl = new URL(String.format("http://localhost:%s", port));

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

        return
                new SpiffEntitlementServiceSdkConfigImpl(
                        baseUrl
                );

    }

}
