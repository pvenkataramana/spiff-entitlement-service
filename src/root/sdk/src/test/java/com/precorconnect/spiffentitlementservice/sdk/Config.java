package com.precorconnect.spiffentitlementservice.sdk;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.identityservice.HmacKey;
import static com.precorconnect.guardclauses.Guards.guardThat;

@Component
public class Config {

    /*
    fields
     */
    private final HmacKey identityServiceJwtSigningKey;
    
    /*
    constructors
     */
    public Config(
    	@NonNull HmacKey identityServiceJwtSigningKey	
    		){
    	
    	this.identityServiceJwtSigningKey =
                guardThat("identityServiceJwtSigningKey",
                        identityServiceJwtSigningKey
                )
                        .isNotNull()
                        .thenGetValue();
    }

    /*
    getter methods
    */
    public HmacKey getIdentityServiceJwtSigningKey() {
        return identityServiceJwtSigningKey;
    }

}
