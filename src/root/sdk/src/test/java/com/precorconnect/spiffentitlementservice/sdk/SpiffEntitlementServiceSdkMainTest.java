package com.precorconnect.spiffentitlementservice.sdk;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public class SpiffEntitlementServiceSdkMainTest {
    /*
    fields
     */

     private final static Dummy dummy =
    		 new Dummy();

    private final static Config config =
            new ConfigFactory()
                    .construct();

    private final static Factory factory =
            new Factory(
                    new IdentityServiceIntegrationTestSdkImpl(
                    		config.getIdentityServiceJwtSigningKey()
                    ),
                    dummy
            );

    public static Collection<Long> createSpiffEntitlement(
    								SpiffEntitlementServiceSdk cilentSdkObject,
    								List<SpiffEntitlementDto> spiffEntitlementDtoList,
    								OAuth2AccessToken accessToken
    ){

    	Collection<Long> ids = new ArrayList<Long>();

			try {
					ids =
						cilentSdkObject.createSpiffEntitlements(
														spiffEntitlementDtoList,
														accessToken
														);
			} catch(Exception e){
				throw new RuntimeException("Exception occured in createSpiffEntitlement: ",e);
			}

		return ids;
    }

    public static Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
    															SpiffEntitlementServiceSdk cilentSdkObject,
    															AccountId accountId,
    															OAuth2AccessToken accessToken
    ){

    	Collection<SpiffEntitlementView> spiffEntitlementViewList = new ArrayList<SpiffEntitlementView>();

    	try {
    		spiffEntitlementViewList =
					cilentSdkObject.listEntitlementsWithPartnerId(
															accountId,
															accessToken
															);
    	} catch(Exception e){
    		throw new RuntimeException("Exception occured in createSpiffEntitlement: ",e);
    	}

    	return spiffEntitlementViewList;
    }

    public static void updateInvoiceUrl(
    						SpiffEntitlementServiceSdk cilentSdkObject,
    						PartnerSaleRegistrationId partnerSaleRegistrationId,
    						InvoiceUrl invoiceUrl,
    						OAuth2AccessToken accessToken
    ){

    	try {

					cilentSdkObject.updateInvoiceUrl(
											partnerSaleRegistrationId,
											invoiceUrl,
											accessToken
											);

    	} catch(Exception e){
    		throw new RuntimeException("Exception occured in updateInvoiceUrl: ",e);
    	}

    }

    public static void updatePartnerRep(
							SpiffEntitlementServiceSdk cilentSdkObject,
							PartnerSaleRegistrationId partnerSaleRegistrationId,
							UserId partnerRepUserId,
							OAuth2AccessToken accessToken
	){

    	try {

			cilentSdkObject.updatePartnerRep(
									partnerSaleRegistrationId,
									partnerRepUserId,
									accessToken
									);

    	} catch(Exception e){
    		throw new RuntimeException("Exception occured in updatePartnerRep: ",e);
    	}

    }

    public static void deleteSpiffEntitlements(
    		SpiffEntitlementServiceSdk cilentSdkObject,
    		List<SpiffEntitlementId> spiffEntitlementIds,
    		OAuth2AccessToken accessToken
    		){

    	try {

				cilentSdkObject
						.deleteSpiffEntitlements(
										spiffEntitlementIds,
										accessToken
										);

		} catch (AuthenticationException | AuthorizationException e) {

			throw new RuntimeException("Exception occured in deleteSpiffEntitlements: ",e);

		}
    }

	public static void main(String args[]) throws MalformedURLException{

		String precorConnectApiBaseUrl=System
	            .getenv("PRECOR_CONNECT_API_BASE_URL");

		SpiffEntitlementServiceSdk cilentSdkObject =
									new SpiffEntitlementServiceSdkImpl(
														new SpiffEntitlementServiceSdkConfigImpl(
																		new URL(precorConnectApiBaseUrl)
																		)
														);

		OAuth2AccessToken accessToken =
								factory
								.constructValidAppOAuth2AccessToken();

		Collection<Long> ids = createSpiffEntitlement(
													cilentSdkObject,
													dummy.getSpiffEntitlementDtosList(),
													accessToken
													);

		listEntitlementsWithPartnerId(
									cilentSdkObject,
									dummy.getSpiffEntitlementDto().getAccountId(),
									accessToken
									);

		updateInvoiceUrl(
					cilentSdkObject,
					dummy.getSpiffEntitlementDto().getPartnerSaleRegistrationId(),
					new InvoiceUrlImpl(dummy.getNewInvoiceUrl()),
					accessToken
					);

		updatePartnerRep(
					cilentSdkObject,
					dummy.getSpiffEntitlementDto().getPartnerSaleRegistrationId(),
					new UserIdImpl(dummy.getNewUserId()),
					accessToken
					);

		List<SpiffEntitlementId> spiffEntitlementIdsList = ids
															.stream()
															.map(id-> new SpiffEntitlementIdImpl(id))
															.collect(Collectors.toList());

		deleteSpiffEntitlements(
						cilentSdkObject,
						spiffEntitlementIdsList,
						accessToken
						);

	}

}
