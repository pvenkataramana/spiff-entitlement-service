package com.precorconnect.spiffentitlementservice.sdk;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public interface SpiffEntitlementServiceSdk {

	 Collection<Long> createSpiffEntitlements(
				@NonNull List<SpiffEntitlementDto> spiffEntitlements,
				@NonNull OAuth2AccessToken accessToken
				) throws AuthenticationException, AuthorizationException;

	 Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
							@NonNull AccountId accountId,
							@NonNull OAuth2AccessToken accessToken
						) throws AuthenticationException, AuthorizationException;

	 void updateInvoiceUrl(
			 @NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			 @NonNull InvoiceUrl invoiceUrl,
			 @NonNull OAuth2AccessToken accessToken
			 ) throws AuthenticationException, AuthorizationException;

	 void updatePartnerRep(
			 @NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			 @NonNull UserId partnerRepUserid,
			 @NonNull OAuth2AccessToken accessToken
			 ) throws AuthenticationException, AuthorizationException;

	 void deleteSpiffEntitlements(
				@NonNull List<SpiffEntitlementId> spiffEntitlementIds,
				@NonNull OAuth2AccessToken oAuth2AccessToken
				) throws AuthenticationException, AuthorizationException;

}
