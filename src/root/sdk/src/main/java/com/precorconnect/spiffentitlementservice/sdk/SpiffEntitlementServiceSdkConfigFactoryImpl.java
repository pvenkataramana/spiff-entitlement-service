package com.precorconnect.spiffentitlementservice.sdk;

import java.net.MalformedURLException;
import java.net.URL;

public class SpiffEntitlementServiceSdkConfigFactoryImpl implements
		SpiffEntitlementServiceSdkConfigFactory {

	@Override
	public SpiffEntitlementServiceSdkConfig construct() {
		return 
				new SpiffEntitlementServiceSdkConfigImpl(
						constructprecorConnectApiBaseUrl()
				);
	}
	
	public URL constructprecorConnectApiBaseUrl(){

		try {

            String precorConnectApiBaseUrl=System
            		            .getenv("PRECOR_CONNECT_API_BASE_URL");

            return
            		new URL(
            		  precorConnectApiBaseUrl
            		);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }
	}
}
