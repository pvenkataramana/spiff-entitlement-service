package com.precorconnect.spiffentitlementservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.URL;

import org.checkerframework.checker.nullness.qual.NonNull;

public class SpiffEntitlementServiceSdkConfigImpl implements
		SpiffEntitlementServiceSdkConfig {
	
    /*
    fields
     */
    private final URL precorConnectApiBaseUrl;

    /*
    constructors
     */
    public SpiffEntitlementServiceSdkConfigImpl(

            @NonNull final URL precorConnectApiBaseUrl
    ) {

    	this.precorConnectApiBaseUrl =
                guardThat(
                        "precorConnectApiBaseUrl",
                        precorConnectApiBaseUrl
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */

    @Override
	public URL getPrecorConnectApiBaseUrl() {
        return precorConnectApiBaseUrl;

    }

}
