package com.precorconnect.spiffentitlementservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;

@Singleton
public class DeleteSpiffEntitlementsFeatureImpl
		implements DeleteSpiffEntitlementsFeature {

    /*
    fields
     */
    private final WebTarget spiffEntitlementsWebTarget;

    @Inject
    public DeleteSpiffEntitlementsFeatureImpl(
    		 @NonNull final WebTarget spiffEntitlementsWebTarget
    ) {

    	this.spiffEntitlementsWebTarget =
                guardThat(
                        "spiffEntitlementsWebTarget",
                        spiffEntitlementsWebTarget
                )
                        .isNotNull()
                        .thenGetValue().path("/spiff-entitlements");

    }

	@Override
	public void deleteSpiffEntitlements(
			@NonNull List<SpiffEntitlementId> spiffEntitlementIds,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			) throws AuthenticationException, AuthorizationException {

		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        oAuth2AccessToken.getValue()
                );

        try {

            	spiffEntitlementsWebTarget
            				.path("/spiffentitlementids")
                            .request(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", authorizationHeaderValue)
                            .post(
                            		Entity.entity(
                            				spiffEntitlementIds
                            							.stream()
                            							.map(id-> id.getValue())
                            							.collect(Collectors.toList()),
                                            MediaType.APPLICATION_JSON_TYPE
                                    )
                            );

        } catch (NotAuthorizedException e) {

            throw new AuthenticationException(e);

        }

	}

}
