package com.precorconnect.spiffentitlementservice.sdk;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public interface ListSpiffEntitlementFeature {

	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			) throws AuthenticationException, AuthorizationException;

}
