package com.precorconnect.spiffentitlementservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.URISyntaxException;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.glassfish.jersey.apache.connector.ApacheClientProperties;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.jackson.JacksonFeature;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final SpiffEntitlementServiceSdkConfig config;

    /*
    constructors
     */
    @Inject
    public GuiceModule(
            @NonNull final SpiffEntitlementServiceSdkConfig config
    ) {

    	this.config =
                guardThat(
                        "config",
                        config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure(
    ) {

        bindFactories();

        bindFeatures();

    }

    private void bindFactories() {

        bind(SpiffEntitlementsSdkResponseFactory.class)
                .to(SpiffEntitlementsSdkResponseFactoryImpl.class);


        bind(SpiffEntitlementsSdkRequestFactory.class)
        		.to(SpiffEntitlementsSdkRequestFactoryImpl.class);
    }

    private void bindFeatures() {

        bind(ListSpiffEntitlementFeature.class)
                .to(ListSpiffEntitlementFeatureImpl.class);

        bind(CreateSpiffEntitlementFeature.class)
        		.to(CreateSpiffEntitlementFeatureImpl.class);

        bind(UpdateInvoiceUrlFeature.class)
        		.to(UpdateInvoiceUrlFeatureImpl.class);

        bind(UpdatePartnerRepFeature.class)
        		.to(UpdatePartnerRepFeatureImpl.class);

        bind(DeleteSpiffEntitlementsFeature.class)
				.to(DeleteSpiffEntitlementsFeatureImpl.class);

    }

    @Provides
    @Singleton
    WebTarget constructWebTarget(
            @NonNull final ObjectMapperProvider objectMapperProvider
    ) {

        try {
        	ClientConfig clientConfig = new ClientConfig();
         	clientConfig.property(ApacheClientProperties.CONNECTION_MANAGER, new PoolingHttpClientConnectionManager());
         	clientConfig.connectorProvider(new ApacheConnectorProvider());

            return
                    ClientBuilder
                            .newClient(clientConfig)
                            .property(ClientProperties.CONNECT_TIMEOUT, 3000)
                            .property(ClientProperties.READ_TIMEOUT,    5000)
                            .register(objectMapperProvider)
                            .register(JacksonFeature.class)
                            .target(config.getPrecorConnectApiBaseUrl().toURI());

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }

    }

}
