package com.precorconnect.spiffentitlementservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.webapiobjectmodel.SpiffEntitlementWebDto;


@Singleton
public class CreateSpiffEntitlementFeatureImpl
		implements CreateSpiffEntitlementFeature {

    /*
    fields
     */
    private final WebTarget spiffEntitlementsWebTarget;

    private final SpiffEntitlementsSdkRequestFactory spiffEntitlementsSdkRequestFactory;

    @Inject
    public CreateSpiffEntitlementFeatureImpl(
    		 @NonNull final WebTarget spiffEntitlementsWebTarget,
    		 @NonNull final SpiffEntitlementsSdkRequestFactory spiffEntitlementsSdkRequestFactory
    ) {

    	this.spiffEntitlementsWebTarget =
                guardThat(
                        "spiffEntitlementsWebTarget",
                        spiffEntitlementsWebTarget
                )
                        .isNotNull()
                        .thenGetValue().path("/spiff-entitlements");

    	this.spiffEntitlementsSdkRequestFactory =
                guardThat(
                        "spiffEntitlementsSdkRequestFactory",
                        spiffEntitlementsSdkRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<Long> createSpiffEntitlements(
											@NonNull List<SpiffEntitlementDto> spiffEntitlementDtoList,
											@NonNull OAuth2AccessToken oAuth2AccessToken
										) throws AuthenticationException, AuthorizationException {

		Collection<Long> spiffEntitlementIds;

		List<SpiffEntitlementWebDto> spiffEntitlementWebDtoList = new ArrayList<SpiffEntitlementWebDto>();

		for(SpiffEntitlementDto spiffEntitlementDto: spiffEntitlementDtoList){

			SpiffEntitlementWebDto spiffEntitlementWebDto =
									spiffEntitlementsSdkRequestFactory
									.construct(spiffEntitlementDto);
			spiffEntitlementWebDtoList.add(spiffEntitlementWebDto);

		}

		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        oAuth2AccessToken.getValue()
                );

        try {

        	spiffEntitlementIds =
            		spiffEntitlementsWebTarget
                            .request(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", authorizationHeaderValue)
                            .post(
                            		Entity.entity(
                            				spiffEntitlementWebDtoList,
                                            MediaType.APPLICATION_JSON_TYPE
                                    ),
                             new GenericType<Collection<Long>>() {
                            		}
                            );

        } catch (NotAuthorizedException e) {

            throw new AuthenticationException(e);

        }

        return
        		spiffEntitlementIds;

	}
}
