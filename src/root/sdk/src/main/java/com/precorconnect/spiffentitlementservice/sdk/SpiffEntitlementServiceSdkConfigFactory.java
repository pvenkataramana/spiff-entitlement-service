package com.precorconnect.spiffentitlementservice.sdk;

public interface SpiffEntitlementServiceSdkConfigFactory {

	SpiffEntitlementServiceSdkConfig construct();
	
}
