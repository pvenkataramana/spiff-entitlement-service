package com.precorconnect.spiffentitlementservice.sdk;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;

public interface UpdatePartnerRepFeature {

	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId delaerRepUserid,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			) throws AuthenticationException, AuthorizationException;

}
