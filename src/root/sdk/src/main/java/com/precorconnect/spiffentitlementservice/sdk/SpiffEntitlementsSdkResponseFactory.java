package com.precorconnect.spiffentitlementservice.sdk;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffentitlementservice.webapiobjectmodel.SpiffEntitlementWebView;

public interface SpiffEntitlementsSdkResponseFactory {

	public SpiffEntitlementView construct(
  		  						@NonNull SpiffEntitlementWebView spiffEntitlementView
								);
}
