package com.precorconnect.spiffentitlementservice.core;

import static org.mockito.Mockito.doNothing;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;

@RunWith(MockitoJUnitRunner.class)
public class UpdatePartnerRepFeatureImplTest {
	/*
    fields
     */
	@Mock
	private DatabaseAdapter databaseAdapter;

	@InjectMocks
	private UpdatePartnerRepFeatureImpl updatePartnerRepFeatureImpl;

	private Dummy dummy = new Dummy();

	@Test
	public void testupdatePartnerRep_whenPartnerSaleRegId_shownNothing() throws AuthenticationException, AuthorizationException{

		doNothing()
		.when(databaseAdapter)
				.updatePartnerRep(
						dummy.getPartnerSaleRegistrationId(),
						dummy.getNewPartnerRepUserId()
						);

		updatePartnerRepFeatureImpl
						.updatePartnerRep(
								dummy.getPartnerSaleRegistrationId(),
								dummy.getNewPartnerRepUserId()
								);

	}
}
