package com.precorconnect.spiffentitlementservice.core;

import static org.mockito.Mockito.doNothing;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;

@RunWith(MockitoJUnitRunner.class)
public class UpdateInvoiceUrlFeatureImplTest {
	/*
    fields
     */
	@Mock
	private DatabaseAdapter databaseAdapter;

	@InjectMocks
	private UpdateInvoiceUrlFeatureImpl updateInvoiceUrlFeatureImpl;

	private Dummy dummy = new Dummy();

	@Test
	public void testupdateInvoiceUrl_whenPartnerSaleRegId_shownNothing() throws AuthenticationException, AuthorizationException {

		doNothing()
		.when(databaseAdapter)
				.updateInvoiceUrl(
						dummy.getPartnerSaleRegistrationId(),
						dummy.getNewInvoiceUrl()
						);

		updateInvoiceUrlFeatureImpl
						.updateInvoiceUrl(
								dummy.getPartnerSaleRegistrationId(),
								dummy.getNewInvoiceUrl()
								);

	}

}
