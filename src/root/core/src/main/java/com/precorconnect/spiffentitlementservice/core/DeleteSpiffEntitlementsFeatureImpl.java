package com.precorconnect.spiffentitlementservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;

@Singleton
public class DeleteSpiffEntitlementsFeatureImpl implements
		DeleteSpiffEntitlementsFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public DeleteSpiffEntitlementsFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }
	@Override
	public void deleteSpiffEntitlements(
			@NonNull List<SpiffEntitlementId> spiffEntitlementIds) throws AuthenticationException, AuthorizationException {

		databaseAdapter
			.deleteSpiffEntitlements(
					spiffEntitlementIds
					);

	}

}
