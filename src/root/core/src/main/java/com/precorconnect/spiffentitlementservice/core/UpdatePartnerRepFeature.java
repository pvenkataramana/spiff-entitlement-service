package com.precorconnect.spiffentitlementservice.core;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.UserId;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;

public interface UpdatePartnerRepFeature {

	 void updatePartnerRep(
			 @NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			 @NonNull UserId partnerRepUserId
			) throws AuthenticationException, AuthorizationException;

}
