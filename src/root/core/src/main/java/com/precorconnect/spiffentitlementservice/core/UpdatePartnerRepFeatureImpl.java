package com.precorconnect.spiffentitlementservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.UserId;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;

@Singleton
public class UpdatePartnerRepFeatureImpl implements UpdatePartnerRepFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public UpdatePartnerRepFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId partnerRepUserId
			) throws AuthenticationException, AuthorizationException {

        databaseAdapter.updatePartnerRep(
        					partnerSaleRegistrationId,
        					partnerRepUserId
        					);

	}


}
