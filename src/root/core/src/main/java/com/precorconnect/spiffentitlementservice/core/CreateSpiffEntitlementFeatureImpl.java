package com.precorconnect.spiffentitlementservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;

@Singleton
public class CreateSpiffEntitlementFeatureImpl implements
		CreateSpiffEntitlementFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public CreateSpiffEntitlementFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<SpiffEntitlementId> createSpiffEntitlements(
				@NonNull List<SpiffEntitlementDto> spiffEntitlements
				) throws AuthenticationException, AuthorizationException{

		return
                databaseAdapter.createSpiffEntitlements(spiffEntitlements);

	}

}
