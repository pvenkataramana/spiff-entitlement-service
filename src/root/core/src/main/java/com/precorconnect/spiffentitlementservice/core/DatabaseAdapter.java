package com.precorconnect.spiffentitlementservice.core;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.UserId;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public interface DatabaseAdapter {

	 Collection<SpiffEntitlementId> createSpiffEntitlements(
				@NonNull List<SpiffEntitlementDto> spiffEntitlementDtos
				);

	 Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
							@NonNull AccountId accountId
							);

	 void updateInvoiceUrl(
			 	@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			 	@NonNull InvoiceUrl invoiceUrl
			 	);

	 void updatePartnerRep(
			 	@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			 	@NonNull UserId delaerRepUserid
			 	);

	 void deleteSpiffEntitlements(
			 	@NonNull List<SpiffEntitlementId> spiffEntitlementIds
			 	);

}
