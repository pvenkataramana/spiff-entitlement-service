package com.precorconnect.spiffentitlementservice.core;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public interface ListSpiffEntitlementFeature {

    Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
    											@NonNull AccountId accountId
												) throws AuthenticationException, AuthorizationException;

}
