package com.precorconnect.spiffentitlementservice.core;

import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;

public interface DeleteSpiffEntitlementsFeature {

	void deleteSpiffEntitlements(
			@NonNull List<SpiffEntitlementId> spiffEntitlementIds
			) throws AuthenticationException, AuthorizationException;
}
