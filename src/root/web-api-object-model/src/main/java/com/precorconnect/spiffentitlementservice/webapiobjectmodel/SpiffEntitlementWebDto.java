package com.precorconnect.spiffentitlementservice.webapiobjectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class SpiffEntitlementWebDto {

	/*
    fields
     */
    private final String accountId;

    private final Long partnerSaleRegistrationId;

    private final String facilityName;

    private final String invoiceNumber;

    private final String invoiceUrl;

    private final String partnerRepUserId;

    private final String installDate;

    private final Double spiffAmount;

    /*
    constructors
     */
    public SpiffEntitlementWebDto(){

    	accountId = null;
    	partnerSaleRegistrationId = 0L;
    	facilityName = null;
    	invoiceNumber = null;
    	invoiceUrl = null;
    	partnerRepUserId = null;
    	installDate = null;
    	spiffAmount = 0.0;

    }

    public SpiffEntitlementWebDto(
    		@NonNull final String accountId,
            @NonNull final Long partnerSaleRegistrationId,
            @NonNull final String facilityName,
            @NonNull final String invoiceNumber,
            @NonNull final String invoiceUrl,
            @NonNull final String partnerRepUserId,
            @NonNull final String installDate,
            @NonNull final Double spiffAmount
    ) {

    	this.accountId =
                guardThat(
                        "accountId",
                        accountId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                        partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();
    	this.facilityName =
                guardThat(
                        "facilityName",
                        facilityName
                )
                        .isNotNull()
                        .thenGetValue();

    	this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                        invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();
    	this.invoiceUrl =
                guardThat(
                        "invoiceUrl",
                        invoiceUrl
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerRepUserId =
                guardThat(
                        "partnerRepUserId",
                        partnerRepUserId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.installDate =
                guardThat(
                        "installDate",
                        installDate
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffAmount =
                guardThat(
                        "spiffAmount",
                        spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
    */
	public String getAccountId() {
		return accountId;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public String getInvoiceUrl() {
		return invoiceUrl;
	}

	public String getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public String getInstallDate() {
		return installDate;
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}


}
