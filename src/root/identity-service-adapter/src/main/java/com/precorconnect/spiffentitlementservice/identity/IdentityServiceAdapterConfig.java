package com.precorconnect.spiffentitlementservice.identity;

import java.net.URL;

public interface IdentityServiceAdapterConfig {

    URL getPrecorConnectApiBaseUrl();

}
