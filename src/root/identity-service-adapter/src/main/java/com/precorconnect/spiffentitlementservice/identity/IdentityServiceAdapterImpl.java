package com.precorconnect.spiffentitlementservice.identity;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerRepAccessContext;
import com.precorconnect.spiffentitlementservice.core.IdentityServiceAdapter;

public class IdentityServiceAdapterImpl implements
        IdentityServiceAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public IdentityServiceAdapterImpl(
           @NonNull IdentityServiceAdapterConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(config);

        injector =
                Guice.createInjector(guiceModule);
    }

    @Override
    public PartnerRepAccessContext getPartnerRepAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException {

        return
                injector
                        .getInstance(GetPartnerRepAccessContextFeature.class)
                        .execute(accessToken);

    }

}
