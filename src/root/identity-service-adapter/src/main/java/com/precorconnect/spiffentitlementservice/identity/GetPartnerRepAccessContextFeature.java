package com.precorconnect.spiffentitlementservice.identity;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerRepAccessContext;

public interface GetPartnerRepAccessContextFeature {

    PartnerRepAccessContext execute(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;
    
}
